name = "Colm"
age = 67
temperature = 98.9
email_for_colm = "colm@google.com" # providing my email to google.
a = 36
b= "Hello"
name = "Zoey"
greeting = "Time for the park"

print(a+3)  # expected result is 6
print(b + "3")  # you can add a string to a string but not a number and a string
name_4_colm = "named variable" # snake case. can use camelCase also
print(name)
print(age)
print(greeting +" " + name)

name = "Zendena"
print(greeting + " " + name)

print(f'{greeting}{name}') # f strings the proper way to concatenate strings

full_greeting = f'{greeting}{name}'

print(full_greeting)
print(temperature)