from playwright.sync_api import Playwright, sync_playwright, expect


def run(playwright: Playwright) -> None:
    browser = playwright.chromium.launch(headless=False, slow_mo=500)  #slowmo slows down the tests for debugging
    context = browser.new_context()
    page = context.new_page()


    page.goto("https://practicetestautomation.com/practice-test-login/")
    page.wait_for_load_state # default timeout is about 30 seconds. Change with .click(timeout=x)
    page.get_by_label("Username").click()
    page.get_by_label("Username").fill("student")
    page.get_by_label("Username").press("Tab")
    page.get_by_label("Password").fill("Password123")
    page.get_by_role("button", name="Submit").click()
    page.goto("https://practicetestautomation.com/logged-in-successfully/")
    expect(page.get_by_role("button", name="Submit")).to_be_hidden()
    print("Yay!")
    # page.pause()  pauses the test. go to the playwright inspector and debug.

    page.goto("https://practicetestautomation.com/practice-test-login/")

    # ---------------------
    context.close()
    browser.close()


with sync_playwright() as playwright:
    run(playwright)
